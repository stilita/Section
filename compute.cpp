#include "compute.h"


Compute::Compute(MKmatrices* mat)
{
    matrices = mat;
}

void Compute::run(void){

    start = std::chrono::system_clock::now();

    P = partitionMatrix();

//    #pragma omp parallel num_threads(2)
//    {
//        if(omp_get_thread_num() == 0){
            permuteM();
//        }else{
            permuteK();
//        }
//        #pragma omp barrier
//    }

    solvePhiR();


    std::cout << "Fixed points for coordinates..." << std::endl;

    p1 = {0.0, 0.0, 0.0};
    p2 = {1.0, 0.0, 0.0};


    #pragma omp parallel num_threads(2)
    {
        if(omp_get_thread_num() == 0){
            H1 = generateH(p1, matrices->mesh->R1set);
            C1 = decomposeQR(H1);
        }else{
            H2 = generateH(p2, matrices->mesh->R2set);
            C2 = decomposeQR(H2);
        }
        #pragma omp barrier
    }


    // testMSE();

    H_MSE = MSEmatrix();
    H_hat = Hhat(1.0);

    MassProperties();

    // Eigen::Vector3d ea = {1.0, 0.0, 0.0};

    ElasticProperties((p2-p1).norm(), (p2-p1).normalized() );
}

Eigen::SparseMatrix<double,Eigen::ColMajor> Compute::partitionMatrix(void){

    int n_dof = matrices->mesh->meshNodes*matrices->mesh->meshDim;

    std::vector<T> tripletList;

    tripletList.reserve(n_dof);

    // create Permutation matrix
    // First add R
    for(int i=0; i<matrices->mesh->Rcoord.size(); i++){

        tripletList.push_back(T(i,matrices->mesh->Rcoord[i],1));
    }

    // Then add L
    for(int i=0; i<+matrices->mesh->Lcoord.size(); i++){
        tripletList.push_back(T(i+matrices->mesh->Rcoord.size(),matrices->mesh->Lcoord[i],1));
    }

    // create permutation matrix
    Eigen::SparseMatrix<double> P(n_dof, n_dof);

    P.setFromTriplets(tripletList.begin(), tripletList.end());

    return P;
}
    // permutations:

void Compute::permuteM(){

    std::cout << "Permute matrices to split into R and L dofs" << std::endl;

    auto M = P*matrices->MM*(P.transpose());

    printElapsed("Permutation");

    // block sizes
    auto Rsize = matrices->mesh->Rcoord.size();
    auto Lsize = matrices->mesh->Lcoord.size();

    #pragma omp parallel num_threads(3)
    {
        if(omp_get_thread_num() == 0){
            std::cout << "Extract M_RR" << std::endl;
            M_RR = M.topLeftCorner(Rsize,  Rsize);
        }else if(omp_get_thread_num() == 1){
            std::cout << "Extract M_RL" << std::endl;
            M_RL = M.topRightCorner(Rsize, Lsize);
        }else{
            std::cout << "Extract M_LL" << std::endl;
            M_LL = M.bottomRightCorner(Lsize, Lsize);
        }
        #pragma omp barrier
        // matrices->clearM();
    }
}

void Compute::permuteK(){

    std::cout << "Permute matrices to split into R and L dofs" << std::endl;

    auto K = P*matrices->KK*(P.transpose());

    printElapsed("Permutation");

    // block sizes
    auto Rsize = matrices->mesh->Rcoord.size();
    auto Lsize = matrices->mesh->Lcoord.size();

    #pragma omp parallel num_threads(3)
    {
        if(omp_get_thread_num() == 0){
            std::cout << "Extract K_RR" << std::endl;
            K_RR = K.topLeftCorner(Rsize, Rsize);
        }else if(omp_get_thread_num() == 1){
            std::cout << "Extract K_LR" << std::endl;
            K_LR = K.bottomLeftCorner(Lsize, Rsize);
        }else{
            std::cout << "Extract K_LL" << std::endl;
            K_LL = K.bottomRightCorner(Lsize, Lsize);
        }
        #pragma omp barrier
        //matrices->clearK();
    }
    // std::cout << "Extract K_RL" << std::endl;
    // K_RL = K.topRightCorner(Rsize, Lsize);
    //std::cout << "Extract M_LR" << std::endl;
    // M_LR = M.bottomLeftCorner(Lsize, Rsize);

    printElapsed("Extracting Matrices");

}

void Compute::solvePhiR(){

    // block sizes
    auto Rsize = matrices->mesh->Rcoord.size();
    auto Lsize = matrices->mesh->Lcoord.size();

#if 0
    Eigen::SimplicialLLT<Eigen::SparseMatrix<double> > cholesky;
#else
    Eigen::CholmodSupernodalLLT<Eigen::SparseMatrix<double>, 1 > cholesky;
#endif

    printElapsed("Building Solver");

    cholesky.analyzePattern(K_LL);

    printElapsed("Analyzing pattern");

    cholesky.factorize(K_LL);

    printElapsed("Factorizing");

    cholesky.compute(K_LL);

    printElapsed("Computing");

    Eigen::MatrixXd Phi_R(K_LL.rows(), K_LR.cols());

    // solve K_LL * K_LR = Phi_R
    Phi_R = cholesky.solve(K_LR);

    printElapsed("Solving");

    // change sign:
    Phi_R = -Phi_R;


    // std::cout << Phi_R << std::endl;

    //std::cout << Eigen::MatrixXd(K_LL) << std::endl;
    //std::cout << Eigen::MatrixXd(cholesky.matrixL()) << std::endl;


    Eigen::MatrixXd M_RL_Phi_R(Rsize, Rsize);
    Eigen::MatrixXd Mat1(Rsize, Rsize);

    M_RL_Phi_R = M_RL*Phi_R;


    Mat1 = Phi_R.transpose() * M_LL* Phi_R;
    M_BB = M_RL_Phi_R.transpose(); //
    M_BB = M_BB + Mat1;
    M_BB = M_BB + M_RL_Phi_R;
    M_BB = M_BB + M_RR;

    K_BB = K_RR + K_LR.transpose()*Phi_R;

}



Eigen::SparseMatrix<double, Eigen::ColMajor> Compute::generateH(Eigen::Vector3d center, std::vector<unsigned int> Rset){

    Eigen::SparseMatrix<double, Eigen::ColMajor> H(Rset.size()*3,6);
    double currCoord[3];


    std::vector<T> tripletList;
    tripletList.reserve(9*Rset.size());


    unsigned long currentNodeinH = 0;


//    #pragma omp parallel
    {
//        #pragma omp for
        for(auto currR1Node = Rset.begin(); currR1Node != Rset.end(); ++currR1Node){
            matrices->mesh->getCoordinatesOfNode(*currR1Node, currCoord);

            Eigen::Vector3d point = {currCoord[0], currCoord[1], currCoord[2]};

            // compute coordinates wrt center of section Eq 2.16
            point -= center;


            for(int i=0; i<3; ++i){
                // first 3 elements: eye 3x3
                tripletList.push_back(Eigen::Triplet<double>(3*currentNodeinH+i, i, 1.0));
            }

            // cross product

            // Avoid adding coordinates that are zero
            double thresh = 1.0e-14;

            if(fabs(point(0)) > thresh){
                tripletList.push_back(Eigen::Triplet<double>(3*currentNodeinH+2, 4, -point(0)));
                tripletList.push_back(Eigen::Triplet<double>(3*currentNodeinH+1, 5, point(0)));
            }

            if(fabs(point(1)) > thresh){
                tripletList.push_back(Eigen::Triplet<double>(3*currentNodeinH, 5, -point(1)));
                tripletList.push_back(Eigen::Triplet<double>(3*currentNodeinH+2, 3, point(1)));
            }

            if(fabs(point(2)) > thresh){
                tripletList.push_back(Eigen::Triplet<double>(3*currentNodeinH, 4, point(2)));
                tripletList.push_back(Eigen::Triplet<double>(3*currentNodeinH+1, 3, -point(2)));
            }

            currentNodeinH++;
        }

        H.setFromTriplets(tripletList.begin(), tripletList.end());

    }
    // std::cout << "Decompose: " << std::endl;

    // std::cout << H << std::endl;

    return H;
}


Eigen::MatrixXd Compute::decomposeQR(Eigen::SparseMatrix<double> H){

    std::cout << "Compute QR decomposition" << std::endl;

    // Eigen::MatrixXf pippo = H1.toDense();

    Eigen::SparseQR <Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> solver;
    solver.compute(H);
    if(solver.info() != Eigen::Success) {
        // decomposition failed
        std::cerr << "QR Decomposition failed" << std::endl;

        exit(-1);
    }else{
        std::cout << "QR Decomposition succeded" << std::endl;
        Eigen::MatrixXd tmpC;

        tmpC = solver.matrixQ();

        tmpC = tmpC.block(0, 6, tmpC.rows(), tmpC.cols()-6);

        std::cout << "Non-zeros: " << tmpC.nonZeros() << std::endl;
        std::cout << "Rows: " << tmpC.rows() << std::endl;
        std::cout << "Columns: " << tmpC.cols() << std::endl;

        return tmpC;

    }

}


Eigen::MatrixXd Compute::MSEmatrix(void){


    Eigen::MatrixXd C1TKbb11(C1.cols(), C1.cols());
    Eigen::MatrixXd C1TKbb12(C1.cols(), C2.rows());
    Eigen::MatrixXd C2TKbb21(C2.cols(), C1.rows());
    Eigen::MatrixXd C2TKbb22(C2.cols(), C2.rows());


    Eigen::MatrixXd A(C1.cols(), C1.cols());
    Eigen::MatrixXd B(C1.rows(), C2.rows());
    Eigen::MatrixXd C(C2.rows(), C1.rows());
    Eigen::MatrixXd D(C2.cols(), C2.cols());

    Eigen::MatrixXd invA(C1.cols(), C1.cols());
    Eigen::MatrixXd invD(C2.cols(), C2.cols());

    Eigen::MatrixXd invCTKbbC11(C1.rows(), C1.rows());
    Eigen::MatrixXd invCTKbbC12(C1.rows(), C2.rows());
    Eigen::MatrixXd invCTKbbC21(C2.rows(), C1.rows());
    Eigen::MatrixXd invCTKbbC22(C2.rows(), C2.rows());


    Eigen::MatrixXd CTKbbH11(C1.cols(), H1.cols());
    Eigen::MatrixXd CTKbbH12(C1.cols(), H2.cols());
    Eigen::MatrixXd CTKbbH21(C2.cols(), H1.cols());
    Eigen::MatrixXd CTKbbH22(C2.cols(), H2.cols());

    Eigen::MatrixXd localH_MSE(H1.rows()+H2.rows(), H1.cols()+H2.cols());
    // compute block operation of C.T * K_BB


    #pragma omp parallel num_threads(4)
    {
        if(omp_get_thread_num() == 0){

            std::cout << "Compute C1^T *K_BB_11" << std::endl;

            C1TKbb11 = C1.transpose() * K_BB.topLeftCorner(C1.rows(), C1.rows());

            A = C1TKbb11 * C1;

        }else if(omp_get_thread_num() == 1){

            std::cout << "Compute C1^T *K_BB_12" << std::endl;

            C1TKbb12 = C1.transpose() * K_BB.topRightCorner(C1.rows(), C2.rows());

            B = C1TKbb12 * C2;

        }else if(omp_get_thread_num() == 2){

            std::cout << "Compute C2^T *K_BB_21" << std::endl;

            C2TKbb21 = C2.transpose() * K_BB.bottomLeftCorner(C2.rows(), C1.rows());

            C = C2TKbb21 * C1;

        }else{

            std::cout << "Compute C2^T *K_BB_22" << std::endl;

            C2TKbb22 = C2.transpose() * K_BB.bottomRightCorner(C2.rows(), C2.rows());

            D = C2TKbb22 * C2;
        }
        #pragma omp barrier
    }
    // compute inv(C.T * K_BB * C) as block matrix

#ifdef _VERBOSE_
    std::cout << "Compute A = C1TKbb11 * C1" << std::endl;

    std::cout << "K_BB Rows: " << K_BB.rows() << std::endl;
    std::cout << "K_BB Columns: " << K_BB.cols() << std::endl;


    std::cout << "C1 Rows: " << C1.rows() << std::endl;
    std::cout << "C1 Columns: " << C1.cols() << std::endl;
    std::cout << "C2 Rows: " << C2.rows() << std::endl;
    std::cout << "C2 Columns: " << C2.cols() << std::endl;
    std::cout << "C1TKbb11 Rows: " << C1TKbb11.rows() << std::endl;
    std::cout << "C1TKbb11 Columns: " << C1TKbb11 .cols() << std::endl;
    std::cout << "C1TKbb12 Rows: " << C1TKbb12.rows() << std::endl;
    std::cout << "C1TKbb12 Columns: " << C1TKbb12 .cols() << std::endl;
    std::cout << "C2TKbb21 Rows: " << C2TKbb21.rows() << std::endl;
    std::cout << "C2TKbb21 Columns: " << C2TKbb21 .cols() << std::endl;
    std::cout << "C2TKbb22 Rows: " << C2TKbb22.rows() << std::endl;
    std::cout << "C2TKbb22 Columns: " << C2TKbb22 .cols() << std::endl;

    std::cout << "(" << C1TKbb11.rows() << ", " << C1TKbb11.cols() << " ) x (" << C1.rows() << ", " << C1.cols() << ")" << std::endl;

    std::cout << "C1:" << std::endl;

    std::cout << C1 << std::endl;

    std::cout << "C1TKbb11:" << std::endl;

    std::cout << C1TKbb11 << std::endl;
#endif



    #pragma omp parallel num_threads(2)
    {
        if (omp_get_thread_num() == 0){

            std::cout << "compute inverse of A" << std::endl;

            invA = A.inverse();

            std::cout << "compute the block inverse of CT Kbb C (1,1) and (1,2)" << std::endl;

            invCTKbbC11 = A - B * invD * C;
            invCTKbbC11 = invCTKbbC11.inverse();

            invCTKbbC12 = -invCTKbbC11 * B * invD;


        }else{

            std::cout << "compute inverse of D " << std::endl;

            invD = D.inverse();

            std::cout << "compute the block inverse of CT Kbb C (2,2) and (2,1)" << std::endl;

            invCTKbbC22 = D - C * invA * B;
            invCTKbbC22 = invCTKbbC22.inverse();

            invCTKbbC21 = -invCTKbbC22 * C * invA;

        }
        #pragma omp barrier
    }

    std::cout << "compute  CT Kbb H" << std::endl;

    #pragma omp parallel num_threads(4)
    {
        if (omp_get_thread_num() == 0){

            CTKbbH11 = C1TKbb11 * H1;

        }else if(omp_get_thread_num() == 1){

            CTKbbH12 = C1TKbb12 * H2;

        }else if(omp_get_thread_num() == 2){

            CTKbbH21 = C2TKbb21 * H1;

        }else{

            CTKbbH22 = C2TKbb22 * H2;

        }
        #pragma omp barrier
    }

    std::cout << "H_MSE" << std::endl;

    #pragma omp parallel num_threads(4)
    {
        if (omp_get_thread_num() == 0){

            localH_MSE.topLeftCorner(H1.rows(), H1.cols())    = C1 * (invCTKbbC11 * CTKbbH11 + invCTKbbC12 * CTKbbH21);

        }else if(omp_get_thread_num() == 1){

            localH_MSE.topRightCorner(H1.rows(), H2.cols())   = C1 * (invCTKbbC11 * CTKbbH12 + invCTKbbC12 * CTKbbH22);

        }else if(omp_get_thread_num() == 2){

            localH_MSE.bottomLeftCorner(H2.rows(), H1.cols()) = C2 * (invCTKbbC21 * CTKbbH11 + invCTKbbC22 * CTKbbH21);

        }else{

            localH_MSE.bottomRightCorner(H2.rows(), H2.cols())= C2 * (invCTKbbC21 * CTKbbH12 + invCTKbbC22 * CTKbbH22);

        }
        #pragma omp barrier
    }


    std::cout << "H_MSE computed" << std::endl;
    return localH_MSE;
}


Eigen::MatrixXd Compute::Hhat(double alpha){

    Eigen::MatrixXd localHhat(H1.rows()+H2.rows(), H1.cols()+H2.cols());

    std::cout << "Compute H_hat using alpha = " << alpha << std::endl;

    #pragma omp parallel num_threads(4)
    {
        if (omp_get_thread_num() == 0){

            localHhat.topLeftCorner(H1.rows(), H1.cols())    = H1 - alpha * H_MSE.topLeftCorner(H1.rows(), H1.cols());

        }else if(omp_get_thread_num() == 1){

            localHhat.topRightCorner(H1.rows(), H2.cols())   = -alpha * H_MSE.topRightCorner(H1.rows(), H2.cols());

        }else if(omp_get_thread_num() == 2){

            localHhat.bottomLeftCorner(H2.rows(), H1.cols()) = -alpha * H_MSE.bottomLeftCorner(H2.rows(), H1.cols());

        }else{

            localHhat.bottomRightCorner(H2.rows(), H2.cols())= H2 - alpha * H_MSE.bottomRightCorner(H2.rows(), H2.cols());

        }
        #pragma omp barrier
    }

    std::cout << "H_hat computed" << std::endl;

    return localHhat;


}


void Compute::MassProperties(void){

    Eigen::MatrixXd Zrb = Eigen::MatrixXd::Zero(12,6);

    Zrb.topLeftCorner(6,6).setIdentity();
    Zrb.bottomRightCorner(6,6).setIdentity();

    Eigen::Vector3d Deltap = p2 - p1;

    Zrb(6,4) =  Deltap(2);
    Zrb(6,5) = -Deltap(1);
    Zrb(7,5) =  Deltap(0);

    Zrb(7,3) = -Deltap(2);
    Zrb(8,3) =  Deltap(1);
    Zrb(8,4) = -Deltap(0);

    //std::cout << "Zrb: " << std::endl;
    //std::cout << Zrb << std::endl;

    Eigen::MatrixXd Mrb = Zrb.transpose() * H_hat.transpose() * M_BB * H_hat * Zrb;

    // std::cout << Mrb << std::endl;
    mass = Mrb(0,0);

    p_cm(0) = (Mrb(5,1)-Mrb(4,2))/(2.0*mass);
    p_cm(1) = (Mrb(3,2)-Mrb(5,0))/(2.0*mass);
    p_cm(2) = (Mrb(4,0)-Mrb(3,1))/(2.0*mass);

    Jcm = Mrb.bottomRightCorner(3,3)-mass*(p_cm.squaredNorm()*Eigen::MatrixXd::Identity(3,3) - p_cm * p_cm.transpose() );



    std::cout << "Mass: " << mass << std::endl;
    std::cout << "center of mass: \n" << p_cm << std::endl;

    std::cout << "Jcm: \n" << Jcm << std::endl;

}



void Compute::ElasticProperties(double length, Eigen::Vector3d ea){

    Eigen::MatrixXd Kr = H_hat.transpose() * K_BB * H_hat;

    Eigen::MatrixXd el_mat = Kr.bottomRightCorner(6,6).inverse();

    Eigen::Matrix3d A  = el_mat.topLeftCorner(3,3);
    Eigen::Matrix3d E  = el_mat.bottomRightCorner(3,3);
    Eigen::Matrix3d B  = el_mat.topRightCorner(3,3);
    Eigen::Matrix3d Bt = el_mat.bottomLeftCorner(3,3);

    Eigen::Matrix3d ea_cross = Eigen::MatrixXd::Zero(3,3);

    ea_cross(0,1) = -ea(2);
    ea_cross(0,2) = ea(1);
    ea_cross(1,0) = ea(2);
    ea_cross(1,2) = -ea(0);
    ea_cross(2,0) = -ea(1);
    ea_cross(2,1) = ea(0);

    Eigen::Matrix3d Gmm = 1.0/length * E;
    Eigen::Matrix3d Gfm = 1.0/length * B - 0.5 * ea_cross.transpose() * E;
    Eigen::Matrix3d Gff = 1.0/length * A - 0.5*(ea_cross.transpose() * Bt + B * ea_cross) + length/6.0 * ea_cross.transpose() * E * ea_cross;

    Eigen::MatrixXd G = Eigen::MatrixXd::Zero(6,6);

    G.topLeftCorner(3,3)     = Gff;
    G.topRightCorner(3,3)    = Gfm;
    G.bottomLeftCorner(3,3)  = Gfm.transpose();
    G.bottomRightCorner(3,3) = Gmm;

    D = G.inverse();

    std::cout << "Elastic matrix:" << std::endl;
    std::cout << D << std::endl;

}


void Compute::printElapsed(std::string message){

    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;


    std::time_t end_time = std::chrono::system_clock::to_time_t(end);
    std::cout << "finished " << message << " at " << std::ctime(&end_time)
              << "elapsed time: " << elapsed_seconds.count() << "s"
              << std::endl;
    start = end;
}





void Compute::testMSE(void){

    std::cout << "WARNING: test method. Remove from constructor" << std::endl;


    // Eigen::MatrixXd KBB = Eigen::MatrixXd::Random(72,72);
    // Eigen::MatrixXd KBB = K_BB;

    // Eigen::MatrixXd C1 = Eigen::MatrixXd::Random(36,30);
    // Eigen::MatrixXd C2 = Eigen::MatrixXd::Random(36,30);





    Eigen::MatrixXd C1TKbb11(C1.cols(), C1.cols());
    Eigen::MatrixXd C1TKbb12(C1.cols(), C2.rows());
    Eigen::MatrixXd C2TKbb21(C2.cols(), C1.rows());
    Eigen::MatrixXd C2TKbb22(C2.cols(), C2.rows());


    Eigen::MatrixXd A(C1.cols(), C1.cols());
    //Eigen::SparseMatrix<double, Eigen::ColMajor> C1TKbb12(C1.rows(), C2.rows());
    //Eigen::SparseMatrix<double, Eigen::ColMajor> C2TKbb21(C2.rows(), C1.rows());
    Eigen::MatrixXd D(C2.cols(), C2.cols());




    // compute block operation of C.T * K_BB




    std::cout << "Compute C1^T *K_BB_11" << std::endl;

    C1TKbb11 = C1.transpose() * K_BB.topLeftCorner(C1.rows(), C1.rows());

    std::cout << "Compute C1^T *K_BB_12" << std::endl;

    C1TKbb12 = C1.transpose() * K_BB.topRightCorner(C1.rows(), C2.rows());

    std::cout << "Compute C2^T *K_BB_21" << std::endl;

    C2TKbb21 = C2.transpose() * K_BB.bottomLeftCorner(C2.rows(), C1.rows());

    std::cout << "Compute C2^T *K_BB_22" << std::endl;

    C2TKbb22 = C2.transpose() * K_BB.bottomRightCorner(C2.rows(), C2.rows());

    // compute inv(C.T * K_BB * C) as block matrix

    std::cout << "Compute A = C1TKbb11 * C1" << std::endl;

    std::cout << "K_BB Rows: " << K_BB.rows() << std::endl;
    std::cout << "K_BB Columns: " << K_BB.cols() << std::endl;

#ifdef _VERBOSE_
    std::cout << "C1 Rows: " << C1.rows() << std::endl;
    std::cout << "C1 Columns: " << C1.cols() << std::endl;
    std::cout << "C2 Rows: " << C2.rows() << std::endl;
    std::cout << "C2 Columns: " << C2.cols() << std::endl;
    std::cout << "C1TKbb11 Rows: " << C1TKbb11.rows() << std::endl;
    std::cout << "C1TKbb11 Columns: " << C1TKbb11 .cols() << std::endl;
    std::cout << "C1TKbb12 Rows: " << C1TKbb12.rows() << std::endl;
    std::cout << "C1TKbb12 Columns: " << C1TKbb12 .cols() << std::endl;
    std::cout << "C2TKbb21 Rows: " << C2TKbb21.rows() << std::endl;
    std::cout << "C2TKbb21 Columns: " << C2TKbb21 .cols() << std::endl;
    std::cout << "C2TKbb22 Rows: " << C2TKbb22.rows() << std::endl;
    std::cout << "C2TKbb22 Columns: " << C2TKbb22 .cols() << std::endl;

    std::cout << "(" << C1TKbb11.rows() << ", " << C1TKbb11.cols() << " ) x (" << C1.rows() << ", " << C1.cols() << ")" << std::endl;

    std::cout << "C1:" << std::endl;

    std::cout << C1 << std::endl;

    std::cout << "C1TKbb11:" << std::endl;

    std::cout << C1TKbb11 << std::endl;
#endif

    A = C1TKbb11 * C1;

    std::cout << "A:" << std::endl;

    std::cout << A << std::endl;


    D = C2TKbb22 * C2;

    std::cout << "D:" << std::endl;

    std::cout << D << std::endl;







/*
    Eigen::MatrixXd C1TKBB(C1.cols(),C1.rows());
    Eigen::MatrixXd C1TKBBC1(C1.cols(),C1.cols());


    std::cout << "C1: " << std::endl;
    std::cout << C1 << std::endl;


    std::cout << "KBB: " << std::endl;
    std::cout << KBB << std::endl;


    C1TKBB = C1.transpose() * KBB.block(0, 0, C1.rows(), C1.rows());

    std::cout << "C1TKBB: " << std::endl;
    std::cout << C1TKBB << std::endl;



    C1TKBBC1 = C1TKBB * C1;

    std::cout << "C1TKBBC1: " << std::endl;
    std::cout << C1TKBBC1 << std::endl;
*/

    std::cout << "test on MSE finished. Exiting..." << std::endl;

    // exit(0);

}


void Compute::testPartition(void){

    int n = 10;

    //typedef Eigen::Triplet<double> T;

    std::vector<T> tripletList, tlP;

    tripletList.reserve(n);
    tlP.reserve(n);


    // create diagonal matrix
    for(int i=0; i<n; i++){
      tripletList.push_back(T(i,i,i+0.5));
    }
    Eigen::SparseMatrix<double> mat(n,n);
    mat.setFromTriplets(tripletList.begin(), tripletList.end());

    // print matrix
    std::cout << "Initial diagonal matrix" << std::endl;
    std::cout << mat << std::endl;

    // create test permutation matrix
    std::vector<int> P1 = {3,1,5,2,0,6,4,7,9,8};

    for(int i=0; i<n; i++){
      tlP.push_back(T(i,P1[i],1));
    }

    Eigen::SparseMatrix<double> P(n,n);
    P.setFromTriplets(tlP.begin(), tlP.end());

    // print permutation
    std::cout << "Permutation Matrix" << std::endl;
    std::cout << P << std::endl;

    // print permuted matrix
    std::cout << "P*A: permutation of rows" << std::endl;
    std::cout << P*mat << std::endl;

    std::cout << "A*P: permutation of columns" << std::endl;
    std::cout << mat*(P.transpose()) << std::endl;

    std::cout << "P*A*P: full partition" << std::endl;
    std::cout << P*mat*(P.transpose()) << std::endl;


}

