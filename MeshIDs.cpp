#include<numeric>
#include <algorithm>
#include "MeshIDs.h"

MeshIDs::MeshIDs(std::string fileName){
    readMeshData(fileName);
    generatePartitions();

}

void MeshIDs::readMeshData(std::string fileName){

    std::vector<std::string> names;

    try {
         myMedMesh=MEDCoupling::MEDFileUMesh::New(fileName);
    } catch (INTERP_KERNEL::Exception) {
        std::cerr << "Unable to open mesh file. Exiting..." << std::endl;
        exit(-1);
    }

    try {
        names = MEDCoupling::GetMeshNames(fileName);
    } catch (INTERP_KERNEL::Exception) {
        std::cerr << "Unable to get mesh names. Exiting..." << std::endl;
        exit(-1);
    }

    std::cout << "Name of Meshes..." << std::endl;

    for (auto i: names)
        std::cout << i << ' ';
    std::cout << std::endl;

    try {
        std::cout << "Looking for R1 group..." << std::endl;
        R1 = myMedMesh->getGroup(1, "R1");
    } catch (INTERP_KERNEL::Exception) {
        std::cerr << "Unable to find R1 group" << std::endl;
    }

    try {
        std::cout << "Looking for R2 group..." << std::endl;
        R2 = myMedMesh->getGroup(1, "R2");
    } catch (INTERP_KERNEL::Exception) {
        std::cerr << "Unable to find R2 group" << std::endl;
    }

    std::cout << "Nodes and elements:" << std::endl;

    meshNodes = myMedMesh->getNumberOfNodes();
    meshDim = myMedMesh->getMeshDimension();

    std::cout << "Mesh dimension: " << meshDim << std::endl;
    std::cout << "Mesh number of nodes: " << meshNodes << std::endl;

    // set pointer to node coordinates
    nodeCoords = myMedMesh->getCoords();

}

void MeshIDs::generatePartitions(){

    MEDCoupling::DataArrayInt64 *ids_R1 =  myMedMesh->getNodeGroupArr("R1");
    MEDCoupling::DataArrayInt64 *ids_R2 =  myMedMesh->getNodeGroupArr("R2");

    R1set.reserve(ids_R1->getNbOfElems());
    R2set.reserve(ids_R2->getNbOfElems());

    // fill R1 and R2 with nodes belonging to the sets
    for(auto a = ids_R1->begin(); a != ids_R1->end(); a++){
        R1set.push_back(*a);
    }

    for(auto a = ids_R2->begin(); a != ids_R2->end(); a++){
        R2set.push_back(*a);
    }

    // generate a temporary vector with all nodes (0 -> n-1)
    std::vector<unsigned int> allNodes(meshNodes);
    // create a range of sequentially increasing values
    std::iota(allNodes.begin(), allNodes.end(), 0);

    // create a temporary vector for the first difference nodes - R1
    std::vector<unsigned int> firstDiff;

    // reserve the exact amount of nodes for Lset
    Lset.reserve(meshNodes-(ids_R1->getNbOfElems() + ids_R2->getNbOfElems()));

    // make first difference nodes - R1
    std::set_difference(allNodes.begin(), allNodes.end(), R1set.begin(), R1set.end(), std::inserter(firstDiff, firstDiff.begin()));
    // make second difference
    std::set_difference(firstDiff.begin(), firstDiff.end(), R2set.begin(), R2set.end(), std::inserter(Lset, Lset.begin()));

#ifdef _VERBOSE_
    std::cout << "R1 set : " << std::endl;
    printVector(R1set);

    std::cout << "R2 set : " << std::endl;
    printVector(R2set);

    std::cout << "L set : " << std::endl;
    printVector(Lset);
#endif

    R1coord.reserve(R1set.size()*meshDim);
    R2coord.reserve(R2set.size()*meshDim);
    Rcoord.reserve(R1set.size()*meshDim + R2set.size()*meshDim);
    Lcoord.reserve(Lset.size()*meshDim);
    populateCoords(&R1set, &R1coord);
    populateCoords(&R2set, &R2coord);
    populateCoords(&Lset, &Lcoord);

    Rcoord.insert(Rcoord.begin(), R1coord.begin(), R1coord.end());
    Rcoord.insert(Rcoord.end(), R2coord.begin(), R2coord.end());

#ifdef _VERBOSE_
    std::cout << "R1 coord : " << std::endl;
    printVector(R1coord);
    std::cout << "R2 coord : " << std::endl;
    printVector(R2coord);
    std::cout << "R coord : " << std::endl;
    printVector(Rcoord);
    std::cout << "L coord : " << std::endl;
    printVector(Lcoord);

#endif

}

void MeshIDs::populateCoords(const std::vector<unsigned int> *set, std::vector<unsigned int> *coord){
    for(auto &element : *set ){
        // std::cout << "current element : " << element << std::endl;
        for (unsigned int c=0; c<3; ++c){
            // std::cout << "current coord : " << element*3+c << std::endl;
            coord->push_back(3*element+c);
        }
    }
}

template<typename T>
void MeshIDs::printVector(std::vector<T> const &input){
    for (auto const &i: input) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

void MeshIDs::getCoordinatesOfNode(MEDCoupling::mcIdType nodeId, double* coords){

    // std::cout << "Number of elements" << nodeCoords->getNbOfElems() << std::endl;
    // std::cout << "Number of Components" << nodeCoords->getNumberOfComponents() << std::endl;
    // std::cout << "Number of Tuples" <<nodeCoords->getNumberOfTuples() << std::endl;

    nodeCoords->getTuple(nodeId, coords);

#ifdef _VERBOSE_
    std::cout << "Node " << nodeId << " - x: " << coords[0] << " y: " << coords[1] << " z: " << coords[2] << std::endl;
#endif
    //for(auto a = nodeCoords->begin(); a != nodeCoords->end(); a++){
    //    std::cout << *a << std::endl;
    //}

}
