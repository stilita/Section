
#include <getopt.h>
#include <iostream>
#include <vector>
#include <omp.h>

#include "MeshIDs.h"
#include "mkmatrices.h"
#include "compute.h"

void printHelp(char* app)
{
  fprintf(stderr,
    "Section properties computation from MED file\n"
    "Usage: %s [OPTION]...\n"
    "Example: %s -h\n"
    "\n"
    "  -h        Print out this help\n"
    "\n"
    "  -f PATH   Specify the path to the MED file\n"
    "\n",
    app,
    app
  );
}

void printShapeData(cnpy::NpyArray *shape){

    assert(shape->shape.size() == 1);
    assert(shape->shape[0] == 2);
    assert(shape->word_size == sizeof(long));

    long *data = shape->data<long>();

    std::cout << "rows    : " << data[0] << std::endl;
    std::cout << "columns : " << data[1] << std::endl;

}

void printFormatData(cnpy::NpyArray *format){

    assert(format->shape.size() == 0);
    assert(format->word_size == 3);

    char *data = format->data<char>();

    std::cout << "format    : " << data[0]  << data[1] << data[2] << std::endl;

}


void printCnpyData(cnpy::NpyArray *data, std::string name){

    std::cout << "shape of " << name <<  ": " << data->shape.size() << std::endl;
    std::cout << "size 0 of " << name << ": " << data->shape[0] << std::endl;
    std::cout << "size 1 of " << name << ": " << data->shape[1] << std::endl;

    std::cout << "word size of " << name << ": " << data->word_size << std::endl;



}


int main(int argc, char *argv[])
{

    Eigen::initParallel();

    Eigen::setNbThreads(8);

    std::string fileName;

    char c;

    while((c = getopt (argc, argv, "f:h")) != -1){
        switch(c){

        case '?':
        case 'h':
        default :
            printHelp(argv[0]);
            return -1;
        case -1:
            break;
        case 'f':
            fileName = std::string(optarg);
            std::cout << "Mesh " << fileName<< " will be used." << std::endl;
        }
    }

    // read IDs from MED file
    MeshIDs *myMeshData = new MeshIDs(fileName);

    // read M K from npz files
    MKmatrices *MK = new MKmatrices(myMeshData, "./Mass.npz", "./Stiff.npz");

    Compute *comp = new Compute(MK);

    comp->run();

    // comp->testPartition();

    return 0;
}
