#include "mkmatrices.h"

MKmatrices::MKmatrices(MeshIDs *meshData, std::string Mfile, std::string Kfile)
{
    // pointer to MeshIDs
    mesh = meshData;

    std::cout << "Load mass matrix" << std::endl;
    MM = loadMK(Mfile);

    std::cout << "Load stiffness matrix" << std::endl;
    KK = loadMK(Kfile);
}


Eigen::SparseMatrix<double, Eigen::RowMajor>
MKmatrices::loadMK(std::string name)
{
    auto cnpyMat = cnpy::npz_load(name);

    // data, format, indices, indptr, shape

    std::cout << "Loading shape... " << std::endl;
    auto M_shape   = cnpyMat["shape"];

    std::cout << "Loading format... " << std::endl;
    auto M_format = cnpyMat["format"];

    std::cout << "Loading indptr... " << std::endl;
    auto M_indptr  = cnpyMat["indptr"];

    std::cout << "Loading indices... " << std::endl;
    auto M_indices = cnpyMat["indices"];

    std::cout << "Loading data... " << std::endl;
    auto M_data    = cnpyMat["data"];

    // printCnpyData(&M_shape, "M_shape");
    // printShapeData(&M_shape);

    //printCnpyData(&M_format, "M_format");
    // printFormatData(&M_format);

    // printCnpyData(&M_indices, "M_indices");

    // printCnpyData(&M_indptr, "M_indptr");

    // printCnpyData(&M_data, "M_data");

    long *shape    = M_shape.data<long>();
    int  *innerptr = M_indices.data<int>();
    int  *outerptr = M_indptr.data<int>();
    double *data   = M_data.data<double>();
    long nnz       = M_data.num_vals;

    // Eigen::Map<Eigen::SparseMatrix<std::complex<double>, Eigen::RowMajor> > mat(3, 3, 3, row_pointer, columns, values);
    // M(rows, cols, number_of_nonzeros (nnz), outer_index (rows), inner_index (cols), values

    Eigen::SparseMatrix<double, Eigen::RowMajor> mat = Eigen::Map<Eigen::SparseMatrix<double, Eigen::RowMajor>>(shape[0], shape[1], nnz, outerptr, innerptr, data);


    // int nElements = 10;
    // std::cout << mat.block( mat.rows() - nElements, mat.cols() - nElements, nElements, nElements ) << std::endl;

    return mat;
}


void MKmatrices::clearM(){
    MM.resize(0,0);
}

void MKmatrices::clearK(){
    KK.resize(0,0);
}



void MKmatrices::printKeys(cnpy::npz_t *data)
{
    std::vector<std::string> key;

    for(auto it = data->begin(); it != data->end(); ++it) {
      key.push_back(it->first);
      std::cout << "Key: " << it->first << std::endl;
      //std::cout << "Value: " << it->second << std::endl;
    }
}
