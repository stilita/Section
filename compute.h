#ifndef COMPUTE_H
#define COMPUTE_H

#include <omp.h>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <unsupported/Eigen/SparseExtra>
#include<Eigen/SparseCholesky>
#include<Eigen/SparseLU>
//#include <Eigen/PaStiXSupport>
#include <Eigen/CholmodSupport>

#include <Eigen/SparseQR>

#include <chrono>
#include <ctime>
#include <cmath>

#include "MeshIDs.h"
#include "mkmatrices.h"


typedef Eigen::Triplet<double> T;

class Compute
{
public:
    Compute(MKmatrices* matrices);

    void run(void);

    Eigen::SparseMatrix<double,Eigen::ColMajor> partitionMatrix();

    void permuteM(void);
    void permuteK(void);

    void solvePhiR();

    Eigen::SparseMatrix<double, Eigen::ColMajor> generateH(Eigen::Vector3d center, std::vector<unsigned int> Rset);

    Eigen::MatrixXd decomposeQR(Eigen::SparseMatrix<double>);

    Eigen::MatrixXd MSEmatrix(void);

    Eigen::MatrixXd Hhat(double alpha);

    void MassProperties(void);

    void ElasticProperties(double lenght, Eigen::Vector3d ea);

    void testPartition(void);
    void testMSE(void);


private:
    MKmatrices *matrices;
    Eigen::SparseMatrix<double,Eigen::ColMajor> P;
    Eigen::SparseMatrix<double,Eigen::ColMajor> M_RR, M_RL, M_LL; // , M_LR
    Eigen::SparseMatrix<double,Eigen::ColMajor> K_RR, K_LR, K_LL; // , K_RL

    // Eigen::SparseMatrix<double,Eigen::ColMajor> Phi_R;

    Eigen::MatrixXd K_BB, M_BB;

    Eigen::Vector3d p1, p2;

    Eigen::SparseMatrix<double, Eigen::ColMajor> H1, H2;

    Eigen::MatrixXd C1, C2;
    Eigen::MatrixXd H_MSE, H_hat;

    double mass;
    Eigen::Vector3d p_cm;
    Eigen::Matrix3d Jcm;

    Eigen::MatrixXd D;

    // time members and params
    std::chrono::system_clock::time_point start, end;
    void printElapsed(std::string message);
};

#endif // COMPUTE_H
