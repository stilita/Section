#ifndef MESHIDS_H
#define MESHIDS_H


#include <MEDLoader.hxx>
#include <MEDFileMesh.hxx>

#include <cnpy.h>


class MeshIDs{
public:
    MeshIDs(std::string meshName);

    std::vector<unsigned int> R1coord, R2coord, Rcoord, Lcoord;
    std::vector<unsigned int> R1set, R2set, Rset, Lset;

    int meshDim;
    unsigned int meshNodes;

    void getCoordinatesOfNode(MEDCoupling::mcIdType nodeId, double* coords);


private:

    void readMeshData(std::string fileName);

    void generatePartitions(void);

    void populateCoords(const std::vector<unsigned int> *set, std::vector<unsigned int> *coord);

    template<typename T>
    void printVector(std::vector<T> const &input);

    // std::vector<std::string> names;
    // std::string fileName;
    const MEDCoupling::DataArrayDouble* nodeCoords;

    MEDCoupling::MEDFileUMesh *myMedMesh;
    MEDCoupling::MEDCouplingUMesh *R1, *R2;



};

#endif // MESHIDS_H
