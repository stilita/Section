#ifndef MKMATRICES_H
#define MKMATRICES_H


#include <Eigen/Sparse>
#include <Eigen/Core>

#include <cnpy.h>

#include "MeshIDs.h"

class MKmatrices
{
public:
    MKmatrices(MeshIDs *meshData, std::string Mfile, std::string Kfile);

    void clearM();
    void clearK();

private:
    Eigen::SparseMatrix<double, Eigen::RowMajor> loadMK(std::string filename);
    void printKeys(cnpy::npz_t *data);

    MeshIDs *mesh;
    Eigen::SparseMatrix<double, Eigen::RowMajor> MM;
    Eigen::SparseMatrix<double, Eigen::RowMajor> KK;


    friend class Compute;
};

#endif // MKMATRICES_H
